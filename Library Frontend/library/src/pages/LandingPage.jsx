import React, { useEffect, useState } from 'react';
import Landing from '../Components/Landing/LandingButtons';
import { useNavigate, useLocation } from 'react-router-dom';
import LandingButton from "../Components/Landing/LandingButtons";

export default function LandingPage() {
  const navigate = useNavigate();
  const token = localStorage.getItem('token');
  const location = useLocation();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (token && location.pathname === '/') {
      setTimeout(() => {
        navigate('/home'); 
      }, 50); 
    } else {
      setIsLoading(false);
    }
  }, [navigate, token, location]);

  return (
    <div>
      {isLoading ? (
        <div>Loading...</div> 
      ) : (
        <>
          <body>
            <LandingButton/>
          </body>
        </>
      )}
    </div>
  );
}
