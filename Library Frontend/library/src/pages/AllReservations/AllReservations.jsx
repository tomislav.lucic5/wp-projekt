import React from 'react';
import ReservationList from '../../Components/MyReservations/AllReservations';
import Header from '../../Components/Header/Header';
import Footer from '../../Components/Footer/Footer';


export default function AllReservations (){

    return(
        <div>
            <Header />
            <div className="content">
                <ReservationList/>
            </div>
            <Footer />
        </div>
    )
}