import React from 'react';
import Header from '../../Components/Header/Header';
import Publication from '../../Components/PublicationDetails/PublicationDetails'
import Footer from '../../Components/Footer/Footer';
import { Link } from 'react-router-dom';
import './PublicationDetails.css';

export default function PublicationDetails (){

    return(

        <div>
            <Header />
            <div className="content">
                <Publication />
            </div>
            <Footer />
        </div>
    )
}