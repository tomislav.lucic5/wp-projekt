import React from 'react';
import './AddNewReservation.css';
import Header from '../../Components/Header/Header';
import Footer from '../../Components/Footer/Footer';
import AddReservation from '../../Components/MyReservations/AddReservation';

export default function AddNewReservation (){

    return(

        <div>
            <Header/>
            <div className="content">
                <AddReservation/>
            </div>
            <Footer/>
        </div>

    )

}