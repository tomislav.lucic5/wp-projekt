import React from 'react';

import Home from '../Components/Home/Home'
import FilterSearch from '../Components/Home/FilterSearch'
import Header from '../Components/Header/Header';
import Footer from '../Components/Footer/Footer';
import "../Components/Landing/landing.css";

export default function HomePage (){

    return(
        <div>
            <Header/>
            <div className="content">
                <Home/>
                <FilterSearch/>
            </div>
            <Footer/>
        </div>

    )

}