import React from 'react';
import './AddPublication.css';
import Header from '../../../Components/Header/Header';
import AddPublication from '../../../Components/Publication/Components/AddPublication'
import Footer from '../../../Components/Footer/Footer';

export default function AddPublicationPage() {
    return (
        <div>
            <Header />
            <AddPublication />
            <Footer />
        </div>
    );
}
