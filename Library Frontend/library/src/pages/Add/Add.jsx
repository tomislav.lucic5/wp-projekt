import React from 'react';
import './Add.css';
import Header from '../../Components/Header/Header';
import Footer from '../../Components/Footer/Footer';
import { Link } from 'react-router-dom';

export default function Add() {
    return (
        <div>
            <Header />
            <div className='add-content'>
                <p>Please select an option to add new content to the system:</p>
                <Link className="add-buttons" to="/add/reservation">
                    Add Reservation
                </Link>
                <Link className="add-buttons" to="/add/author">
                    Add Author
                </Link>
                <Link className="add-buttons" to="/add/genre">
                    Add Genre
                </Link>
                <Link className="add-buttons" to="/add/publication">
                    Add Publication
                </Link>
            </div>
            <Footer />
        </div>
    );
}
