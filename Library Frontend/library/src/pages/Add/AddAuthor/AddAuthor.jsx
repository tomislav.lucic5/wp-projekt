import React from 'react';
import NewAuthor from "../../../Components/Author/NewAuthor";
import Header from '../../../Components/Header/Header';
import Footer from '../../../Components/Footer/Footer';

export default function AddAuthor (){

    return(

        <div>
            <Header />
            <NewAuthor/>
            <Footer />
        </div>

    )

}