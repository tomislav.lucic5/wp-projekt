import React from 'react';
import './AddGenre.css';
import Header from '../../../Components/Header/Header';
import AddGenre from '../../../Components/Genre/AddGenre'
import Footer from '../../../Components/Footer/Footer';

export default function AddGenrePage (){

    return(

        <div>
            <Header />
            <AddGenre />
            <Footer />
        </div>

    )

}