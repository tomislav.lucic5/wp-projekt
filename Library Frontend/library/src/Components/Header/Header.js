import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import './Header.css';
import LogoutButton from '../Login/LogoutButton';

export default function Header() {
  const role = localStorage.getItem('role');
  const location = useLocation();

  const getActiveClass = (path) => {
    return location.pathname === path ? 'active' : '';
  };

  return (
    <div className="header-container">
      <header className="header">
        <div className="logo-container">
          <Link to="/">
            <img className="logo" src={'/open-book.png'} alt="Logo" />
          </Link>
        </div>
        <div className="header-buttons">
          {role === 'User' && (
            <>
              <Link className={`btn ${getActiveClass('/myreservations')}`} to="/myreservations">
                My Reservations
              </Link>
              <LogoutButton className="btn" />
            </>
          )}
          {role === 'Admin' && (
            <>
              <Link className={`btn ${getActiveClass('/add')}`} to="/add">
                Add
              </Link>
              <Link className={`btn ${getActiveClass('/home/users')}`} to="/home/users">
                User Administration
              </Link>
              <Link className={`btn ${getActiveClass('/myreservations')}`} to="/myreservations">
                My Reservations
              </Link>
              <Link className={`btn ${getActiveClass('/home/allreservations')}`} to="/home/allreservations">
                All Reservations
              </Link>
              <LogoutButton className="btn" />
            </>
          )}
        </div>
      </header>
    </div>
  );
}
