import React, { useState, useEffect } from 'react';
import { useLocation, useNavigate } from "react-router-dom";
import axios from 'axios';
import './PublicationDetails.css';
import Header from '../Header/Header';

export default function PublicationDetails() {
  window.scrollTo({ top: 0, behavior: 'smooth' });
  const [publication, setPublication] = useState(null);
  const [userId, setUserId] = useState(null);
  const location = useLocation();
  const navigate = useNavigate();
  const publicationId = location?.state?.publicationId; 
  const token = localStorage.getItem("token");
    const headers = {
      Authorization: `Bearer ${token}`,
    };
  useEffect(() => {
    
    const fetchPublicationDetails = async () => {
      try {
        const response = await axios.get(`https://localhost:44389/api/library/Publication/${publicationId}`, { headers });
        setPublication(response.data);
        setUserId(response.data.userId);
      } catch (error) {
        console.error('Error fetching publication details:', error);
      }
    };
    fetchPublicationDetails();
  }, [publicationId]);

  const handleReservation = async () => {
    try {
      await axios.post('https://localhost:44389/api/Reservation',  {
        userId,
        publicationId
      }, { headers });
      navigate(-1);
    } catch (error) {
      console.error('Error requesting reservation:', error);
    }
  };

  if (!publication) {
    return <p>Loading...</p>;
  }

  return (
    <div>
      <div className='publication-details'>
        <img src={publication.imageUrl} alt={publication.title} className='publication-image' />
        <div className='publication-info'>
          <h1>{publication.title}</h1>
          <p>{publication.description}</p>
          <ul>
            <li><strong>Number of Pages:</strong> {publication.numberOfPages}</li>
            <li><strong>Genre:</strong> {publication.genre}</li>
            <li><strong>Language:</strong> {publication.language}</li>
            <li><strong>Quantity:</strong> {publication.quantity}</li>
          </ul>
          <button onClick={handleReservation} className='reservation-button' disabled={publication.quantity === 0}>
            REQUEST A RESERVATION
          </button>
        </div>
      </div>
    </div>
  );
}
