import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Pagination from '../Publication/Components/Paging';
import { Link } from "react-router-dom";
import './FilterSearch.css';

export default function FilterSearch() {
  const [filters, setFilters] = useState({
    genre: '',
    type: '',
    author: '',
    minPages: '',
    maxPages: '',
    title: ''
  });

  const [books, setBooks] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [genres, setGenres] = useState([]);
  const [types, setTypes] = useState([]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFilters({
      ...filters,
      [name]: value
    });
  };

  const fetchBooks = async (page) => {
    try {
      const response = await axios.get('https://localhost:44389/api/library/home', {
        params: {
          searchQuery: filters.title,
          minNumberOfPages: filters.minPages,
          maxNumberOfPages: filters.maxPages,
          typeId: filters.type,
          genreId: filters.genre,
          language: '',
          pageSize: 10,
          pageNumber: page,
          orderBy: 'DateCreated',
          sortOrder: 'asc',
        }
      });
      setBooks(response.data.publications);
      setTotalPages(Math.ceil(response.data.totalCount / 10));
      setCurrentPage(response.data.currentPage);
    } catch (error) {
      console.error('Error fetching books:', error);
    }
  };

  useEffect(() => {
    fetchBooks(currentPage);
  }, [currentPage]);

  useEffect(() => {
    const fetchGenres = async () => {
      try {
        const response = await axios.get('https://localhost:44389/api/genre');
        setGenres(response.data);
      } catch (error) {
        console.error('Error fetching genres:', error);
      }
    };

    const fetchTypes = async () => {
      try {
        const response = await axios.get('https://localhost:44389/api/type');
        setTypes(response.data);
      } catch (error) {
        console.error('Error fetching types:', error);
      }
    };

    fetchGenres();
    fetchTypes();
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    setCurrentPage(1);
    fetchBooks(1);
  };

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  return (
    <div className='filter-search-container'>
      <form className='filter-form' onSubmit={handleSubmit}>
        <input
          type='text'
          name='title'
          placeholder='Search by title'
          value={filters.title}
          onChange={handleInputChange}
        />
        <select name='genre' value={filters.genre} onChange={handleInputChange}>
          <option value=''>Select Genre</option>
          {genres.map((genre) => (
            <option key={genre.id} value={genre.id}>
              {genre.name}
            </option>
          ))}
        </select>
        <select name='type' value={filters.type} onChange={handleInputChange}>
          <option value=''>Select Type</option>
          {types.map((type) => (
            <option key={type.id} value={type.id}>
              {type.name}
            </option>
          ))}
        </select>
        <input
          type='number'
          name='minPages'
          placeholder='Min pages'
          value={filters.minPages}
          onChange={handleInputChange}
        />
        <input
          type='number'
          name='maxPages'
          placeholder='Max pages'
          value={filters.maxPages}
          onChange={handleInputChange}
        />
        <button type='submit'>Search</button>
      </form>
      <div className='search-results'>
        {books.length > 0 ? (
          books.map(book => (
            <div className='book-card' key={book.id}>
              <img src={book.imageUrl} alt={`${book.title} cover`} />
              <h3>{book.title}</h3>
              <p>{book.author}</p>
              <Link to={`/publication/${book.id}`} state={{ publicationId: book.id }} className="button">
                Book Info
              </Link>
            </div>
          ))
        ) : (
          <p>No books found</p>
        )}
      </div>
      <Pagination
        currentPage={currentPage}
        totalPages={totalPages}
        onPageChange={handlePageChange}
      />
    </div>
  );
}
