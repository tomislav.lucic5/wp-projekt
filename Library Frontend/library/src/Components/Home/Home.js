import React from 'react';
import './Home.css';
import Header from '../Header/Header';

export default function Home() {

    return (
        <div className='home-container'>
            <div className='user-home'>
                <div className='card-container'>
                    <div className='card'>
                        <img src='/images/search.png' alt='Search for a book' />
                        <h3>Search for a book</h3>
                        <p>Select filters and add a new book to your collection.</p>
                    </div>
                    <div className='card'>
                        <img src='/images/book.png' alt='Choose a book' />
                        <h3>Choose a book</h3>
                        <p>Inform about chosen book on our page.</p>
                    </div>
                    <div className='card'>
                        <img src='/images/request.png' alt='Request a reservation' />
                        <h3>Request a reservation</h3>
                        <p>Request a reservation with one button click.</p>
                    </div>
                    <div className='card'>
                        <img src='/images/pickup.png' alt='Pick up the book' />
                        <h3>Pick up the book</h3>
                        <p>Take a walk and pick the book of your dreams.</p>
                    </div>
                </div>
            </div>
        </div>
    );
}
