import React from 'react';
import './Footer.css';

export default function Footer() {
  return (
    <footer>
      <p className='first-paragraph'>Tomislav Lučić</p>
      <p>2024</p>
    </footer>
  );
}
