import React, { useState, useEffect } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './addPublication.css';

export default function AddPublication() {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [edition, setEdition] = useState('');
  const [datePublished, setDatePublished] = useState(new Date());
  const [quantity, setQuantity] = useState('');
  const [numberOfPages, setNumberOfPages] = useState('');
  const [language, setLanguage] = useState('');
  const [imageUrl, setImageUrl] = useState('');
  const [authors, setAuthors] = useState([]);
  const [genres, setGenres] = useState([]);
  const [types, setTypes] = useState([]);
  const [selectedAuthor, setSelectedAuthor] = useState('');
  const [selectedGenre, setSelectedGenre] = useState('');
  const [selectedType, setSelectedType] = useState('');
  const [successMessage, setSuccessMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    fetchAuthors();
    fetchGenres();
    fetchTypes();
  }, []);

  const fetchAuthors = () => {
    axios.get('https://localhost:44389/api/library/authors')
      .then(response => {
        setAuthors(response.data);
      })
      .catch(error => {
        console.error('Error fetching authors:', error);
      });
  };

  const fetchGenres = () => {
    axios.get('https://localhost:44389/api/genre')
      .then(response => {
        setGenres(response.data);
      })
      .catch(error => {
        console.error('Error fetching genres:', error);
      });
  };

  const fetchTypes = () => {
    axios.get('https://localhost:44389/api/type')
      .then(response => {
        setTypes(response.data);
      })
      .catch(error => {
        console.error('Error fetching types:', error);
      });
  };

  const handleAddPublication = () => {
    if (!title || !description || !edition || !quantity || !numberOfPages || !language || !selectedAuthor || !selectedGenre || !selectedType || !imageUrl) {
      setErrorMessage('Please fill in all required fields.');
      setTimeout(() => setErrorMessage(''), 3000);
      return;
    }

    const newPublication = {
      title,
      description,
      edition,
      datePublished,
      quantity,
      numberOfPages,
      language,
      imageUrl,
      authorId: selectedAuthor,
      genreId: selectedGenre,
      typeId: selectedType
    };

    const token = localStorage.getItem("token"); 
    const headers = {
      Authorization: `Bearer ${token}`,
    };

    axios.post('https://localhost:44389/api/library/add/publication', newPublication, { headers })
      .then(response => {
        console.log('Publication added successfully:', response.data);
        window.scrollTo({ top: 0, behavior: 'smooth' });
        setTitle('');
        setDescription('');
        setEdition('');
        setDatePublished(new Date());
        setQuantity('');
        setNumberOfPages('');
        setLanguage('');
        setImageUrl('');
        setSelectedAuthor('');
        setSelectedGenre('');
        setSelectedType('');
        setSuccessMessage('Publication added successfully!');
        setTimeout(() => setSuccessMessage(''), 3000);
      })
      .catch(error => {
        console.error('Error adding publication:', error);
        setErrorMessage('Failed to add publication. Please try again.');
        window.scrollTo({ top: 0, behavior: 'smooth' });
        setTimeout(() => setErrorMessage(''), 3000);
      });
  };

  return (
    <div className='add-publication'>
      <div className='add-publication-content'>
        <p>Add a new publication</p>

        {successMessage && (
          <div className='success-message'>
            {successMessage}
          </div>
        )}

        {errorMessage && (
          <div className='error-message'>
            {errorMessage}
          </div>
        )}

        <input
          className='add-publication-inputs'
          type='text'
          placeholder='Title'
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          required
        />
        <input
          className='add-publication-inputs'
          type='text'
          placeholder='Description'
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          required
        />
        <input
          className='add-publication-inputs'
          type='text'
          placeholder='Edition'
          value={edition}
          onChange={(e) => setEdition(e.target.value)}
          required
        />
        <DatePicker
          selected={datePublished}
          onChange={(date) => setDatePublished(date)}
          className='add-publication-inputs'
          placeholderText='Date Published'
          required
        />
        <input
          className='add-publication-inputs'
          type='number'
          placeholder='Quantity'
          value={quantity}
          onChange={(e) => setQuantity(e.target.value)}
          required
        />
        <input
          className='add-publication-inputs'
          type='number'
          placeholder='Number of Pages'
          value={numberOfPages}
          onChange={(e) => setNumberOfPages(e.target.value)}
          required
        />
        <input
          className='add-publication-inputs'
          type='text'
          placeholder='Language'
          value={language}
          onChange={(e) => setLanguage(e.target.value)}
          required
        />
        <input
          className='add-publication-inputs'
          type='text'
          placeholder='Image URL'
          value={imageUrl}
          onChange={(e) => setImageUrl(e.target.value)}
          required
        />
        <select
          className='add-publication-inputs'
          value={selectedAuthor}
          onChange={(e) => setSelectedAuthor(e.target.value)}
          required
        >
          <option value=''>Select Author</option>
          {authors.map(author => (
            <option key={author.id} value={author.id}>
              {author.firstName} {author.lastName}
            </option>
          ))}
        </select>
        <select
          className='add-publication-inputs'
          value={selectedGenre}
          onChange={(e) => setSelectedGenre(e.target.value)}
          required
        >
          <option value=''>Select Genre</option>
          {genres.map(genre => (
            <option key={genre.id} value={genre.id}>
              {genre.name}
            </option>
          ))}
        </select>
        <select
          className='add-publication-inputs'
          value={selectedType}
          onChange={(e) => setSelectedType(e.target.value)}
          required
        >
          <option value=''>Select Type</option>
          {types.map(type => (
            <option key={type.id} value={type.id}>
              {type.name}
            </option>
          ))}
        </select>

        <div className='add-publication-buttons-container'>
          <button className='add-publication-buttons' onClick={handleAddPublication}>
            Add
          </button>
        </div>
      </div>
    </div>
  );
}
