import React, { useState } from 'react';
import axios from 'axios';
import './author.css';

export default function NewAuthor() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [dateOfBirth, setDateOfBirth] = useState('');
  const [dateOfDeath, setDateOfDeath] = useState('');
  const [nationality, setNationality] = useState('');
  const [successMessage, setSuccessMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleAddAuthor = () => {
    if (!firstName || !lastName || !dateOfBirth || !nationality) {
      setErrorMessage('Please fill in all required fields.');
      setTimeout(() => setErrorMessage(''), 3000);
      return;
    }

    const newAuthor = {
      firstName,
      lastName,
      dateOfBirth,
      dateOfDeath,
      nationality,
    };

    axios
      .post('https://localhost:44389/api/library/add/author', newAuthor)
      .then((response) => {
        console.log('Author added successfully:', response.data);
        setFirstName('');
        setLastName('');
        setDateOfBirth('');
        setDateOfDeath('');
        setNationality('');
        setSuccessMessage('Author added successfully!');
        setTimeout(() => setSuccessMessage(''), 3000);
      })
      .catch((error) => {
        console.error('Error adding author:', error);
      });
  };

  return (
    <div className="add-author-content edit-inputs">
      <p>Add new author</p>

      {successMessage && (
        <div className="success-message">
          {successMessage}
        </div>
      )}

      {errorMessage && (
        <div className="error-message">
          {errorMessage}
        </div>
      )}

      <input
        className="add-author-inputs"
        type="text"
        placeholder="First Name"
        value={firstName}
        onChange={(e) => setFirstName(e.target.value)}
        required
      />

      <input
        className="add-author-inputs"
        type="text"
        placeholder="Last Name"
        value={lastName}
        onChange={(e) => setLastName(e.target.value)}
        required
      />

      <label className="add-author-labels" htmlFor="dateofbirth">
        Date of Birth:
      </label>
      <input
        className="add-author-inputs"
        type="date"
        id="dateofbirth"
        name="dateofbirth"
        placeholder="Date of Birth"
        value={dateOfBirth}
        onChange={(e) => setDateOfBirth(e.target.value)}
        required
      />

      <label className="add-author-labels" htmlFor="dateofdeath">
        Date of Death:
      </label>
      <input
        className="add-author-inputs"
        type="date"
        id="dateofdeath"
        name="dateofdeath"
        placeholder="Date of Death"
        value={dateOfDeath}
        onChange={(e) => setDateOfDeath(e.target.value)}
      />

      <input
        className="add-author-inputs"
        type="text"
        placeholder="Nationality"
        value={nationality}
        onChange={(e) => setNationality(e.target.value)}
        required
      />

      <div className="add-author-buttons-container edit-buttons">
        <button className="add-author-buttons" onClick={handleAddAuthor}>
          Add
        </button>
      </div>
    </div>
  );
}
