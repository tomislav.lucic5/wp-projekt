import React, { useState } from 'react';
import axios from 'axios';
import './AddGenre.css';

export default function AddGenre() {
  const [title, setTitle] = useState('');
  const [successMessage, setSuccessMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleAddGenre = () => {
    if (!title) {
      setErrorMessage('Title is a required field.');
      setTimeout(() => setErrorMessage(''), 3000);
      return;
    }

    const genreRest = { title }
    const token = localStorage.getItem("token");
    const headers = {
      Authorization: `Bearer ${token}`,
    };

    axios
      .post('https://localhost:44389/api/genre', genreRest, { headers })
      .then((response) => {
        console.log('Genre added successfully:', response.data);
        setTitle('');
        setSuccessMessage('Genre added successfully!');
        setTimeout(() => setSuccessMessage(''), 3000);
      })
      .catch((error) => {
        console.error('Error adding genre:', error);
      });
  };

  return (
    <div className='add-genre'>
      <div className='add-genre-content'>
        <p>Add new genre</p>

        {successMessage && (
          <div className='success-message'>
            {successMessage}
          </div>
        )}

        {errorMessage && (
          <div className='error-message'>
            {errorMessage}
          </div>
        )}

        <input
          className='add-genre-inputs'
          type='text'
          placeholder='Title'
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          required
        />
          
        <div className='add-genre-buttons-container edit-buttons'>
          <button className='add-genre-buttons' onClick={handleAddGenre}>
            Add
          </button>
        </div>
      </div>
    </div>
  );
}
