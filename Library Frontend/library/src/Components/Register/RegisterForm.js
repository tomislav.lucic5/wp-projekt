import React, { useState } from 'react';
import axios from 'axios';
import '../Login/login.css';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router';
import RegistrationService from '../../services/RegistrationService';

function RegisterForm() {

  const handleClick = (e) => {
    e.target.classList.add('button-animation');
  };

  const [user, setUser] = useState({
    firstName: '',
    lastName: '',
    phoneNumber: '',
    address: '',
    email: '',
    password: '',
    DOB: ''
  });

  const [errors, setErrors] = useState({});

  const navigate = useNavigate();

  const handleEmailChange = (event) => {
    setUser({ ...user, email: event.target.value });
  };

  const handlePasswordChange = (event) => {
    setUser({ ...user, password: event.target.value });
  };

  const handleFirstNameChange = (event) => {
    setUser({ ...user, firstName: event.target.value });
  };

  const handleLastNameChange = (event) => {
    setUser({ ...user, lastName: event.target.value });
  };

  const handlePhoneNumberChange = (event) => {
    setUser({ ...user, phoneNumber: event.target.value });
  };

  const handleAddressChange = (event) => {
    setUser({ ...user, address: event.target.value });
  };

  const handleDOBChange = (event) => {
    setUser({ ...user, DOB: event.target.value });
  };

  const validate = () => {
    const errors = {};
    if (!user.firstName) errors.firstName = "First name is required";
    if (!user.lastName) errors.lastName = "Last name is required";
    if (!user.email) errors.email = "Email is required";
    if (!user.DOB) errors.DOB = "Date of Birth is required";
    if (!user.phoneNumber) errors.phoneNumber = "Phone number is required";
    if (!user.address) errors.address = "Address is required";
    if (!user.password) errors.password = "Password is required";
    return errors;
  };

  const handleRegister = async (event) => {
    event.preventDefault();

    const validationErrors = validate();
    if (Object.keys(validationErrors).length > 0) {
      setErrors(validationErrors);
      return;
    }

    try {
      const response = await RegistrationService.postRegister(user);

      console.log('Registration successful:', response.data);

      setUser({
        firstName: '',
        lastName: '',
        phoneNumber: '',
        address: '',
        email: '',
        password: '',
        DOB: ''
      });

      navigate("/");
    } catch (error) {
      console.error('Registration failed:', error);
    }
  };

  return (
    <div className="page-image1">
      <div className="centered-container">
        <div className="register-container">
          <form className="login-div" onSubmit={handleRegister}>
            <input
              type="text"
              placeholder="First Name"
              value={user.firstName}
              onChange={handleFirstNameChange}
            />
            {errors.firstName && <div className="error">{errors.firstName}</div>}
            <input
              type="text"
              placeholder="Last Name"
              value={user.lastName}
              onChange={handleLastNameChange}
            />
            {errors.lastName && <div className="error">{errors.lastName}</div>}
            <input
              type="text"
              placeholder="Email"
              value={user.email}
              onChange={handleEmailChange}
            />
            {errors.email && <div className="error">{errors.email}</div>}
            <input
              type="date"
              placeholder="Date of Birth"
              value={user.DOB}
              onChange={handleDOBChange}
            />
            {errors.DOB && <div className="error">{errors.DOB}</div>}
            <input
              type="text"
              placeholder="Phone Number"
              value={user.phoneNumber}
              onChange={handlePhoneNumberChange}
            />
            {errors.phoneNumber && <div className="error">{errors.phoneNumber}</div>}
            <input
              type="text"
              placeholder="Address"
              value={user.address}
              onChange={handleAddressChange}
            />
            {errors.address && <div className="error">{errors.address}</div>}
            <input
              type="password"
              placeholder="Password"
              value={user.password}
              onChange={handlePasswordChange}
            />
            {errors.password && <div className="error">{errors.password}</div>}
            <button type="submit">Register</button>
            <div onClick={handleClick} className="register-link">Already have an account? <Link to="/login">Login</Link></div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default RegisterForm;
