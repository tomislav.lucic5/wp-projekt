import React, { useState, useEffect } from 'react';
import './login.css';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router';
import RegistrationService from '../../services/RegistrationService';

function LoginForm() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errors, setErrors] = useState({});
  const [errorMessage, setErrorMessage] = useState('');

  const navigate = useNavigate();

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const validate = () => {
    const errors = {};
    if (!email) errors.email = 'Email is required';
    if (!password) errors.password = 'Password is required';
    return errors;
  };

  const handleLogin = async (event) => {
    event.preventDefault();

    const validationErrors = validate();
    if (Object.keys(validationErrors).length > 0) {
      setErrors(validationErrors);
      return;
    }

    try {
      const response = await RegistrationService.postLogin(email, password);

      console.log('Login successful:', response.data);
      localStorage.setItem('token', response.data.accessToken);
      localStorage.setItem('role', response.data.role);
      console.log(localStorage.getItem('token'));
      const userRole = response.data.Role;

      setEmail('');
      setPassword('');

      if (localStorage.getItem('token') != null) {
        navigate('/home');
      }
    } catch (error) {
      console.error('Login failed:', error);
      setErrorMessage(error.response.data.message);
    }
  };

  useEffect(() => {
    const token = localStorage.getItem('token');
    const userRole = localStorage.getItem('role');

    if (token) {
      if (userRole === 'User') {
        navigate('/home');
      } else if (userRole === 'Admin') {
        navigate('/home');
      }
    }
  }, [navigate]);

  return (
    <div className="page-image1">
      <div className="centered-container">
        <div className="login-container">
          <form className="login-div" onSubmit={handleLogin}>
            <input
              type="text"
              placeholder="Email"
              value={email}
              onChange={handleEmailChange}
            />
            {errors.email && <div className="error">{errors.email}</div>}
            <input
              type="password"
              placeholder="Password"
              value={password}
              onChange={handlePasswordChange}
            />
            {errors.password && <div className="error">{errors.password}</div>}
            <button type="submit">Login</button>
          </form>
          {errorMessage && <div className="error-message">{errorMessage}</div>}
          <div className="register-link">
            Don't have an account? <Link to="/register">Register here.</Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoginForm;
