﻿using Library.Model;
using Library.Repository.Common;
using Library.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using Library.Common;

namespace Library.Service
{
    public class AuthorService : IAuthorService
    {
        protected IAuthorRepository AuthorRepository { get; set; }

        public AuthorService(IAuthorRepository authorRepository)
        {
            AuthorRepository = authorRepository;
        }
        
        public async Task<Author> GetAuthorByIdAsync(Guid id)
        {
            return await AuthorRepository.GetAuthorByIdAsync(id);

        }

        public async Task<bool> AddAuthorAsync(string FirstName, string LastName, string Nationality, DateTime DateOfBirth, DateTime? DateOfDeath)
        {
            Author author = new Author()
            {
                Id = Guid.NewGuid(),
                FirstName = FirstName,
                LastName = LastName,
                Nationality = Nationality,
                DateOfBirth = DateOfBirth,
                DateOfDeath = DateOfDeath,
                IsActive = true,
                DateCreated = DateTime.Now
            };

            return await AuthorRepository.AddAuthorAsync(author);

        }
   
        public async Task<PagedList<Author>> GetAllAuthorsAsync(AuthorFiltering filtering, Paging paging, Sorting sorting)
        {
          
            return await AuthorRepository.GetAllAuthorsAsync(filtering, paging, sorting);

        }
    }
}
