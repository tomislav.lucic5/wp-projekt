﻿using Library.Model;
using Library.Repository.Common;
using Library.Service.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Library.Service
{
    public  class GenreService : IGenreService
    {
        protected IGenreRepository GenreRepository { get; set; }
        public GenreService(IGenreRepository genreRepository)
        {
            GenreRepository = genreRepository;
        }

        public async Task<bool> CreateGenreAsync(string genreName)
        {
            ClaimsIdentity identity = System.Web.HttpContext.Current.User.Identity as ClaimsIdentity;
            string userId = identity.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            Genre genre = new Genre()
            {
                Id= Guid.NewGuid(),
                Name = genreName,
                IsActive = true,
                DateCreated = DateTime.Now,
                CreatedByUserId = Guid.Parse(userId)
            };

            return await GenreRepository.CreateGenreAsync(genre);
        }

        public async Task<List<Genre>> GetGenresAsync()
        {
            try
            {
                return await GenreRepository.GetAllGenresAsync();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
    }
}
