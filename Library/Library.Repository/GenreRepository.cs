﻿using Library.Repository.Common;
using MongoDB.Driver.Core.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Library.Common;
using Library.Model;
using System.Diagnostics;

namespace Library.Repository
{
    public class GenreRepository : IGenreRepository
    {
        public async Task<bool> CreateGenreAsync(Genre genre)
        {
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
                {

                    NpgsqlCommand cmd = new NpgsqlCommand("insert into \"Genre\" (\"Id\", \"Name\", \"IsActive\", \"CreatedByUserId\", \"DateCreated\") " +
                        "values (@Id, @Name, @IsActive, @CreatedByUserId, @DateCreated);", conn);
                    if (genre != null)
                    {
                        Guid id = Guid.NewGuid();
                        cmd.Parameters.AddWithValue("@Id", genre.Id);
                        cmd.Parameters.AddWithValue("@Name", genre.Name);
                        cmd.Parameters.AddWithValue("@IsActive", genre.IsActive);
                        cmd.Parameters.AddWithValue("@CreatedByUserId", genre.CreatedByUserId);
                        cmd.Parameters.AddWithValue("@DateCreated", genre.DateCreated);

                        conn.Open();

                        int numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();

                        if (numberOfAffectedRows > 0)
                        {
                            return true;
                        }
                    }
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<List<Genre>> GetAllGenresAsync()
        {
            List<Genre> genres = new List<Genre>();
            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    await connection.OpenAsync();
                    using (NpgsqlCommand cmd = new NpgsqlCommand("SELECT \"Id\", \"Name\" FROM \"Genre\"", connection))
                    {
                        using (NpgsqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                Genre genre = new Genre
                                {
                                    Id = (Guid)reader["Id"],
                                    Name = (string)reader["Name"]
                                };
                                genres.Add(genre);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
            return genres;
        }
    }
}
