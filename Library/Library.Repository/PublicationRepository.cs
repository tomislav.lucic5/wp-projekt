﻿using Library.Common;
using Library.Model;
using Library.Repository.Common;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Claims;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace Library.Repository
{
    public class PublicationRepository : IPublicationRepository
    {

        public async Task<PublicationWithAuthorIds> GetPublicationByIdAsync(Guid id)
       {
            using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
            {
                    string query = @"
                    SELECT p.""Title"", p.""Description"", p.""Edition"", p.""DatePublished"", 
                           p.""Quantity"", p.""NumberOfPages"", p.""Language"", 
                           p.""TypeId"", g.""Name"", p.""ImageUrl""
                    FROM ""Publication"" p
                    JOIN ""Genre"" g ON p.""GenreId"" = g.""Id""
                    WHERE p.""Id"" = @Id";

                NpgsqlCommand cmd = new NpgsqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@Id", id);

                conn.Open();

                NpgsqlDataReader reader = await cmd.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        PublicationWithAuthorIds publication = new PublicationWithAuthorIds
                        {
                            Id = id,
                            Title = reader.GetString(0),
                            Description = reader.GetString(1),
                            Edition = reader.GetInt16(2),
                            DatePublished = reader.GetDateTime(3),
                            Quantity = reader.GetInt16(4),
                            NumberOfPages = reader.GetInt16(5),
                            Language = reader.GetString(6),
                            TypeId = reader.GetGuid(7),
                            Genre = reader.GetString(8),
                            ImageUrl = reader.GetString(9)
                        };

                        ClaimsIdentity identity = System.Web.HttpContext.Current.User.Identity as ClaimsIdentity;
                        string userId = identity.FindFirst(ClaimTypes.NameIdentifier)?.Value;

                        publication.UserId = userId;

                        return publication;
                    }
                }
                return null;
            }

        }

        public async Task<Publication> GetFullPublicationByIdAsync(Guid id)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("select * from \"Publication\"  where \"Id\" = @Id ", conn);
                cmd.Parameters.AddWithValue("@Id", id);

                conn.Open();

                NpgsqlDataReader reader = await cmd.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Publication publication = new Publication()
                        {
                            Id = reader.GetGuid(0),
                            Title = reader.GetString(1),
                            Description = reader.GetString(2),
                            Edition = reader.GetInt16(3),
                            DatePublished = reader.GetDateTime(4),
                            Quantity = reader.GetInt16(5),
                            NumberOfPages = reader.GetInt16(6),
                            Language = reader.GetString(7),
                            TypeId = reader.GetGuid(8),
                            GenreId = reader.GetGuid(9),
                            PublisherId = reader.GetGuid(10),
                            IsActive = reader.GetBoolean(11),
                            CreatedByUserId = reader.GetGuid(12),
                            DateCreated = reader.GetDateTime(13),
                            UpdatedByUserId = reader.GetGuid(14),
                            DateUpdated = reader.GetDateTime(15)
                            
                        };
                        return publication;
                    }
                }
                return null;
            }

        }


        public async Task<bool> AddPublicationAsync(Publication publication, List<Guid> listOfAuthorIds)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
            {
                NpgsqlCommand publicationCmd = new NpgsqlCommand("insert into \"Publication\" (\"Id\", \"Title\", \"Description\", \"Edition\", \"DatePublished\"," +
                    " \"Quantity\", \"NumberOfPages\", \"Language\", \"TypeId\", \"GenreId\", \"IsActive\", \"CreatedByUserId\", \"DateCreated\", \"ImageUrl\") values " +
                    "(@Id, @Title, @Description, @Edition, @DatePublished, @Quantity, @NumberOfPages, @Language, @TypeId, @GenreId, @IsActive, @CreatedByUserId, @DateCreated," +
                    "@ImageUrl); ", conn);

                publicationCmd.Parameters.AddWithValue("@Id", publication.Id);
                publicationCmd.Parameters.AddWithValue("@Title", publication.Title);
                publicationCmd.Parameters.AddWithValue("@Description", publication.Description);
                publicationCmd.Parameters.AddWithValue("@Edition", publication.Edition);
                publicationCmd.Parameters.AddWithValue("@DatePublished", publication.DatePublished);
                publicationCmd.Parameters.AddWithValue("@Quantity", publication.Quantity);
                publicationCmd.Parameters.AddWithValue("@NumberOfPages", publication.NumberOfPages);
                publicationCmd.Parameters.AddWithValue("@Language", publication.Language);
                publicationCmd.Parameters.AddWithValue("@TypeId", publication.TypeId);
                publicationCmd.Parameters.AddWithValue("@GenreId", publication.GenreId);
                publicationCmd.Parameters.AddWithValue("@IsActive", publication.IsActive);
                publicationCmd.Parameters.AddWithValue("@CreatedByUserId", publication.CreatedByUserId);
                publicationCmd.Parameters.AddWithValue("@DateCreated", publication.DateCreated);
                publicationCmd.Parameters.AddWithValue("@ImageUrl", publication.ImageUrl);

                conn.Open();

                int numberOfAffectedRows = await publicationCmd.ExecuteNonQueryAsync();

                return numberOfAffectedRows > 0;
            }
        }

        public async Task<bool> RemovePublicationAsync(Guid id)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("update \"Publication\" set \"IsActive\" = @IsActive  where \"Id\" = @Id", conn);
                //deaktivirati i authorpublication unos
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.Parameters.AddWithValue("@IsActive", false);

                conn.Open();

                int numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();

                if (numberOfAffectedRows > 0)
                {
                    NpgsqlCommand cmd2 = new NpgsqlCommand("update \"PublicationAuthor\" set \"IsActive\" = @IsActive  where \"PublicationId\" = @Id", conn);
                    //deaktivirati i authorpublication unos
                    cmd2.Parameters.AddWithValue("@Id", id);
                    cmd2.Parameters.AddWithValue("@IsActive", false);

                    return true;
                }

                return false;
            }
        }

        public async Task<PublicationWithAuthorIds> UpdatePublicationAsync(Guid id, Publication updatedPublication)
        {
            CultureInfo culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            culture.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd";
            culture.DateTimeFormat.LongTimePattern = "HH:mm:ss";
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;


            try
            {
                int numberOfAffectedRows = 0;
                Publication currentPublication = await GetFullPublicationByIdAsync(id);

                if (currentPublication != null)
                {
                    using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
                    {
                        StringBuilder sb = new StringBuilder();

                        NpgsqlCommand cmd = new NpgsqlCommand();

                        cmd.Connection = conn;

                        sb.Append("update \"Publication\" set ");
                        if (updatedPublication.Title != null & currentPublication.Title != updatedPublication.Title)
                        {
                            sb.Append("\"Title\" = @Title, ");
                            cmd.Parameters.AddWithValue("@Title", updatedPublication.Title);
                        }
                        if (updatedPublication.Description != null & currentPublication.Description != updatedPublication.Description)
                        {
                            sb.Append("\"Description\" = @Description, ");
                            cmd.Parameters.AddWithValue("@Description", updatedPublication.Description);
                        }
                        if (updatedPublication.Edition != 0 & currentPublication.Edition != updatedPublication.Edition)
                        {
                            sb.Append("\"Edition\" = @Edition, ");
                            cmd.Parameters.AddWithValue("@Edition", updatedPublication.Edition);
                        }
                        if (updatedPublication.DatePublished != DateTime.MinValue & currentPublication.DatePublished != updatedPublication.DatePublished)
                        {
                            sb.Append("\"DatePublished\" = @DatePublished, ");
                            cmd.Parameters.AddWithValue("@DatePublished", updatedPublication.DatePublished);
                        }
                        if (updatedPublication.NumberOfPages != 0 & currentPublication.NumberOfPages != updatedPublication.NumberOfPages)
                        {
                            sb.Append("\"NumberOfPages\" = @NumberOfPages, ");
                            cmd.Parameters.AddWithValue("@NumberOfPages", updatedPublication.NumberOfPages);
                        }
                        if (updatedPublication.Language != null & currentPublication.Language != updatedPublication.Language)
                        {
                            sb.Append("\"Language\" = @Language, ");
                            cmd.Parameters.AddWithValue("@Language", updatedPublication.Language);
                        }
                        if (updatedPublication.TypeId != Guid.Empty & currentPublication.TypeId != updatedPublication.TypeId)
                        {
                            NpgsqlCommand checkTypeCmd= new NpgsqlCommand("select * from \"Type\" where \"Id\" = @TypeId",conn);
                            checkTypeCmd.Parameters.AddWithValue("@TypeId", updatedPublication.TypeId);

                            numberOfAffectedRows = await checkTypeCmd.ExecuteNonQueryAsync();
                            if (numberOfAffectedRows > 0)
                            {
                                sb.Append("\"TypeId\" = @TypeId, ");
                                cmd.Parameters.AddWithValue("@TypeId", updatedPublication.TypeId);
                            }
                        }
                        if (updatedPublication.GenreId != Guid.Empty & currentPublication.GenreId != updatedPublication.GenreId)
                        {
                            NpgsqlCommand checkGenreCmd = new NpgsqlCommand("select * from \"Genre\" where \"Id\" = @GenreId", conn);
                            checkGenreCmd.Parameters.AddWithValue("@GenreId", updatedPublication.GenreId);

                            numberOfAffectedRows = await checkGenreCmd.ExecuteNonQueryAsync();
                            if (numberOfAffectedRows > 0)
                            {
                                sb.Append("\"GenreId\" = @GenreId, ");
                                cmd.Parameters.AddWithValue("@GenreId", updatedPublication.GenreId);
                            }
                        }
                        if (updatedPublication.PublisherId != Guid.Empty & currentPublication.PublisherId != updatedPublication.PublisherId )
                        {
                            NpgsqlCommand checkPublisherCmd = new NpgsqlCommand("select * from \"Publisher\" where \"Id\" = @PublisherId", conn);
                            checkPublisherCmd.Parameters.AddWithValue("@PublisherId", updatedPublication.PublisherId);

                            numberOfAffectedRows = await checkPublisherCmd.ExecuteNonQueryAsync();
                            if (numberOfAffectedRows > 0)
                            {
                                sb.Append("\"PublisherId\" = @PublisherId, ");
                                cmd.Parameters.AddWithValue("@PublisherId", updatedPublication.PublisherId);
                            }
                        }
                        if (updatedPublication.IsActive == false)
                        {
                            sb.Append("\"IsActive\" = @IsActive, ");
                            cmd.Parameters.AddWithValue("@IsActive", false);
                        }

                        sb.Append("\"UpdatedByUserId\" = @UpdatedByUserId, ");
                        cmd.Parameters.AddWithValue("@UpdatedByUserId", updatedPublication.UpdatedByUserId);
                        sb.Append("\"DateUpdated\" = @DateUpdated, ");
                        cmd.Parameters.AddWithValue("@DateUpdated", updatedPublication.DateUpdated);

                        sb.Remove(sb.Length - 2, 2);

                        sb.Append(" where \"Id\" = @Id;");
                        cmd.Parameters.AddWithValue("@Id", id);

                        conn.Open();

                        cmd.CommandText = sb.ToString();

                        numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();
                        if (numberOfAffectedRows > 0)
                        {
                            return await GetPublicationByIdAsync(id);
                        }
                    }
                }
                return null;

            }
            catch (Exception)
            {
                return null;
            }

        }

        public async Task<PagedList<PublicationWithAuthorIds>> GetAllPublicationsAsync(PublicationFiltering filtering, Sorting sorting, Paging paging)
        {
            int numberOfRows = 0;

            List<PublicationWithAuthorIds> listOfPublications = new List<PublicationWithAuthorIds>();

            List<string> allowedSortColumns = new List<string> { "title", "edition", "datepublished", "quantity", "numberofpages",
        "Language", "typeid", "genreid", "datecreated" };

            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
                {
                    NpgsqlCommand cmd = new NpgsqlCommand();
                    NpgsqlCommand cmd2 = new NpgsqlCommand();
                    cmd.Connection = conn;
                    cmd2.Connection = conn;

                    StringBuilder queryStringBuilder = new StringBuilder("select \"Id\", \"Title\",\"Description\",\"Edition\",\"DatePublished\",\"Quantity\",\"NumberOfPages\"," +
                            "\"Language\",\"TypeId\",\"GenreId\", \"ImageUrl\" from \"Publication\" ");

                    StringBuilder countStringBuilder = new StringBuilder("select count (*) from \"Publication\" ");

                    queryStringBuilder = PublicationFilterResults(queryStringBuilder, filtering, cmd);
                    countStringBuilder = PublicationFilterResults(countStringBuilder, filtering, cmd2);

                    conn.Open();

                    if (sorting != null && allowedSortColumns.Contains(sorting.OrderBy.ToLower()))
                    {
                        if (allowedSortColumns.Contains(sorting.OrderBy.ToLower()))
                        {
                            queryStringBuilder.Append($" order by \"{sorting.OrderBy}\" ");
                        }
                        if ((sorting.SortOrder.ToLower() == "asc") || (sorting.SortOrder.ToLower() == "desc"))
                        {
                            queryStringBuilder.Append($" {sorting.SortOrder} ");
                        }
                    }

                    if (paging != null)
                    {
                        queryStringBuilder.Append(" offset @Offset rows \r\n fetch next @PageSize rows only");

                        cmd.Parameters.AddWithValue("@Offset", (paging.PageNumber - 1) * paging.PageSize);
                        cmd.Parameters.AddWithValue("@PageSize", paging.PageSize);
                    }

                    cmd.CommandText = queryStringBuilder.ToString();
                    cmd2.CommandText = countStringBuilder.ToString();

                    NpgsqlDataReader reader = await cmd.ExecuteReaderAsync();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            PublicationWithAuthorIds publication = new PublicationWithAuthorIds()
                            {
                                Id = reader.GetGuid(0),
                                Title = reader.GetString(1),
                                Description = reader.GetString(2),
                                Edition = reader.GetInt16(3),
                                DatePublished = reader.GetDateTime(4),
                                Quantity = reader.GetInt16(5),
                                NumberOfPages = reader.GetInt16(6),
                                Language = reader.GetString(7),
                                TypeId = reader.GetGuid(8),
                                GenreId = reader.GetGuid(9),
                                ImageUrl = reader.GetString(10)
                            };

                            listOfPublications.Add(publication);
                        }
                    }

                    await reader.CloseAsync();

                    using (cmd2)
                    {
                        numberOfRows = Convert.ToInt32(await cmd2.ExecuteScalarAsync());
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
            }

            return new PagedList<PublicationWithAuthorIds>(listOfPublications, numberOfRows, paging.PageNumber, paging.PageSize);
        }


        private StringBuilder PublicationFilterResults(StringBuilder filteringStringBuilder, PublicationFiltering filtering, NpgsqlCommand cmd)
        {
            filteringStringBuilder.Append(" WHERE 1 = 1");

            if (filtering != null)
            {
                if (!string.IsNullOrEmpty(filtering.SearchQuery))
                {
                    filteringStringBuilder.Append(" and ((\"Title\" ilike @SearchQuery) or (\"Description\" ilike @SearchQuery))");
                    cmd.Parameters.AddWithValue("@SearchQuery", "%" + filtering.SearchQuery + "%");
                }

                if (filtering.MinNumberOfPages != null)
                {
                    filteringStringBuilder.Append(" and \"NumberOfPages\" >= @MinNumberOfPages ");
                    cmd.Parameters.AddWithValue("@MinNumberOfPages", filtering.MinNumberOfPages);
                }

                if (filtering.MaxNumberOfPages != null)
                {
                    filteringStringBuilder.Append(" and \"NumberOfPages\" <= @MaxNumberOfPages ");
                    cmd.Parameters.AddWithValue("@MaxNumberOfPages", filtering.MaxNumberOfPages);
                }

                if (filtering.TypeId != null)
                {
                    filteringStringBuilder.Append(" and \"TypeId\" = @TypeId ");
                    cmd.Parameters.AddWithValue("@TypeId", filtering.TypeId);
                }

                if (filtering.GenreId != null)
                {
                    filteringStringBuilder.Append(" and \"GenreId\" = @GenreId ");
                    cmd.Parameters.AddWithValue("@GenreId", filtering.GenreId);
                }
            }

            return filteringStringBuilder;
        }


    }

}
