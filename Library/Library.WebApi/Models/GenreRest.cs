﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.WebApi.Models
{
    public class GenreRest
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}